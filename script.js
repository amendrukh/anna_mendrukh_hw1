let x = 6;
let y = 14;
let z = 4;
let result;

// Task 1
result = x += y - x++ * z;
document.write(result + "<br />");

/* 1. x++ * z = 6*4 = 24;
   2. y - x++ * z = 14-24 = -10;
   3. x = x += -10 = 6+(-10) = -4;
 */

//Task 2

z = -x - y * 5;
document.write(z + "<br />");

/* 1. y * 5 = 14*5 = 70;
   2. -x = -4 *(-1) = 4;
   3. z = 4 - 70 = -66;
 */

//Task 3

y /= x + 5 % z;
document.write(y + "<br />");

/* 1.5% z = 5%(-66) = 5;
   2. x + 5% z = -4 + 5 = 1;
   3. y = y /=1 = 14/1 = 14;
*/

//Task 4
result = z - x++ + y * 5;
document.write(result + "<br />");

/* 1. y * 5 = 14 * 5 = 70;
   2. -66 - (-4) + 70 = 8;
 */

//Task 5

x = y - x++ * z;
document.write(x + "<br />");

/* 1. x ++ * z = -3 * (-66) = 198;
      x = -3, because Task 4 --> x++ = -4;
   2. x = 14 - 198 = -184;
*/